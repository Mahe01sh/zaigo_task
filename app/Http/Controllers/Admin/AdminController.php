<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Admin;
use Carbon\Carbon;

class AdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $admins = Admin::latest()->paginate(5);
  
        return view('admins.index',compact('admins'))
            ->with('i', (request()->input('page', 1) - 1) * 5);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admins.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|unique:admins|max:35',
            'email' => 'required|email|regex:/^.+@.+$/i|unique:admins,email',
            'password' => 'required|max:8',
            'media' => 'required|max:5000000|mimes:mp4,jpeg,jpg,png,pdf,xlsx,csv',
            'address' => 'required|regex:/(^[-0-9A-Za-z.,\/ ]+$)/',
            'zipcode' => 'required|numeric|digits:6',
            'mobile' => 'required|numeric|digits:10',
            'date' => 'required|date|after:tomorrow',
        ]);

            $files = $request->file('media');
                
            $mediaFile = date('YmdHis') . "." . $files->getClientOriginalExtension();
            $files->move(public_path().'/media/files/',$mediaFile);            

            Admin::create([
                'name' => $request['name'],
                'email' => $request['email'],
                'password' => Hash::make($request['password']),
                'media' => $mediaFile,
                'address' => $request['address'],
                'zipcode' => $request['zipcode'],
                'mobile' => $request['mobile'],
                'date' => $request['date'],
            ]); 


            return redirect()->route('admins.index')
                                ->with('success','Admin user created successfully.');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Admin $admin)
    {
        return view('admins.show',compact('admin'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Admin $admin)
    {
        return view('admins.edit',compact('admin'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Admin $admin)
    {
        $request->validate([
            'name' => 'required|unique:admins|max:35',
            'email' => 'required|email|regex:/^.+@.+$/i|unique:admins,email',
            'password' => 'required|max:8',
            'media' => 'required|max:5000000|mimes:mp4,jpeg,jpg,png,pdf,xlsx,csv',
            'address' => 'required|regex:/(^[-0-9A-Za-z.,\/ ]+$)/',
            'zipcode' => 'required|numeric|digits:6',
            'mobile' => 'required|numeric|digits:10',
            'date' => 'required|date|after:tomorrow',
        ]);

            $files = $request->file('media');
                
            $mediaFile = date('YmdHis') . "." . $files->getClientOriginalExtension();
            $files->move(public_path().'/media/files/',$mediaFile);            

            Admin::updateOrCreate([
                'name' => $request['name'],
                'email' => $request['email'],
                'password' => Hash::make($request['password']),
                'media' => $mediaFile,
                'address' => $request['address'],
                'zipcode' => $request['zipcode'],
                'mobile' => $request['mobile'],
                'date' => $request['date'],
            ]); 
  
            return redirect()->route('admins.index')
                            ->with('success','Admin user updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Admin $admin)
    {
        $admin->delete();
  
        return redirect()->route('admins.index')
                        ->with('success','Admin user deleted successfully');
    }
}
