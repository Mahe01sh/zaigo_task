<?php

use Illuminate\Database\Seeder;

use App\Admin;

class CreateAdminsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admin = [
            [
               'name'=>'Admin User',
               'email'=>'admin@user.com',
                'is_admin'=>'1',
               'password'=> bcrypt('12345678'),
            ],
            [
               'name'=>'Admin Dev',
               'email'=>'admin@dev.com',
                'is_admin'=>'1',
               'password'=> bcrypt('12345678'),
            ],
        ];
  
        foreach ($admin as $key => $value) {
            Admin::create($value);
        }
    }
}
