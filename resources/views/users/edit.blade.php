@extends('layouts.app')
   
@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Edit User</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-primary" href="{{ route('users.index') }}"> <i class="fa fa-chevron-circle-left" aria-hidden="true"></i></a>
            </div>
        </div>
    </div>
   
    @if ($errors->any())
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
  
    <form action="{{ route('users.update',$user->id) }}" method="POST" enctype="multipart/form-data">
        @csrf
        @method('PUT')
   
         <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Name:</strong>
                    <input type="text" name="name" class="form-control" value="{{ $user->name }}" placeholder="Name">
                    <span class="text-danger">{{ $errors->first('name') }}</span>
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Email:</strong>
                    <input type="text" name="email" class="form-control" value="{{ $user->email }}" placeholder="Email">
                    <span class="text-danger">{{ $errors->first('email') }}</span>
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Password:</strong>
                    <input type="password" name="password" class="form-control" value="{{ $user->password }}" placeholder="Password">
                    <span class="text-danger">{{ $errors->first('password') }}</span>
                </div>
            </div>
            <div class="col-xs-6 col-sm-6 col-md-6">
                <div class="form-group">
                    <strong>Media File:</strong>
                    <input type="file" name="media" class="form-control" value="{{ $user->media }}" placeholder="File">
                    <span class="text-danger">{{ $errors->first('media') }}</span>
                </div>
            </div>
            <div class="col-xs-6 col-sm-6 col-md-6">
                <div class="form-group">
                    <strong>Address:</strong>
                    <select name="address" class="form-control">
                        <option value="{{ $user->address }}" selected>{{ $user->address }}</option>
                        <option value="Chennai">Chennai</option>
                        <option value="Erode">Erode</option>
                        <option value="Salem">Salem</option>
                        <option value="Mayiladuthurai">Mayiladuthurai</option>
                    </select>
                    <span class="text-danger">{{ $errors->first('address') }}</span>
                </div>
            </div>
            <div class="col-xs-6 col-sm-6 col-md-6">
                <div class="form-group">
                    <strong>Mobile:</strong>
                    <input type="text" name="mobile" class="form-control" value="{{ $user->mobile }}" placeholder="Mobile">
                    <span class="text-danger">{{ $errors->first('mobile') }}</span>
                </div>
            </div>
            <div class="col-xs-6 col-sm-6 col-md-6">
                <div class="form-group">
                    <strong>Zip Code:</strong>
                    <input type="text" name="zipcode" class="form-control" value="{{ $user->zipcode }}" placeholder="Zipcode">
                    <span class="text-danger">{{ $errors->first('zipcode') }}</span>
                </div>
            </div>
            <div class="col-xs-6 col-sm-6 col-md-6">
                <div class="form-group">
                    <strong>Date & Time:</strong>
                    <input step="any" type="datetime-local" class="form-control" value="{{ $user->date }}" placeholder="Date">
                    <span class="text-danger">{{ $errors->first('date') }}</span>
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12 text-left">
              <button type="submit" class="btn btn-primary">Submit</button>
            </div>
        </div>
   
    </form>
@endsection
