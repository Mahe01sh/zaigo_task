<?php

use Illuminate\Database\Seeder;

use App\User;

class CreateUsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = [
            [
               'name'=>'Test User',
               'email'=>'test@user.com',
                'is_user'=>'1',
               'password'=> bcrypt('12345678'),
            ],
            [
               'name'=>'Dev User',
               'email'=>'dev@user.com',
                'is_user'=>'1',
               'password'=> bcrypt('12345678'),
            ],
        ];
  
        foreach ($user as $key => $value) {
            User::create($value);
        }
    }
}
