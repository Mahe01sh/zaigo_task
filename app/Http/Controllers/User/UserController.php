<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use App\Admin;

class UserController extends Controller
{
   
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() 
    {
        $users = User::latest()->paginate(5);

        $admins = Admin::latest()->paginate(5);
  
        return view('users.index',compact('users', 'admins'))
            ->with('i', (request()->input('page', 1) - 1) * 5);
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('users.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|unique:admins|max:35',
            'email' => 'required|email|regex:/^.+@.+$/i|unique:admins,email',
            'password' => 'required|max:8',
            'media' => 'required|max:5000000|mimes:mp4,jpeg,jpg,png,pdf,xlsx,csv',
            'address' => 'required|regex:/(^[-0-9A-Za-z.,\/ ]+$)/',
            'zipcode' => 'required|numeric|digits:6',
            'mobile' => 'required|numeric|digits:10',
            'date' => 'required|date|after:tomorrow',
        ]);

        try {

            $files = $request->file('media');
                
            $mediaFile = date('YmdHis') . "." . $files->getClientOriginalExtension();
            $files->move(public_path().'/media/files/',$mediaFile);            

            User::create([
                'name' => $request['name'],
                'email' => $request['email'],
                'password' => Hash::make($request['password']),
                'media' => $mediaFile,
                'address' => $request['address'],
                'zipcode' => $request['zipcode'],
                'mobile' => $request['mobile'],
                'date' => $request['date'],
            ]); 
   
            if($user){
                return redirect()->route('users.index')
                ->with('success','User created successfully.');
            }else{
                return back();
            }


        } catch (ModelNotFoundException $exception) {
            return back()->withError($exception->getMessage())->withInput();
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        return view('users.show',compact('user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        return view('users.edit',compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        $request->validate([
            'name' => 'required|unique:admins|max:35',
            'email' => 'required|email|regex:/^.+@.+$/i|unique:admins,email',
            'password' => 'required|max:8',
            'media' => 'required|max:5000000|mimes:mp4,jpeg,jpg,png,pdf,xlsx,csv',
            'address' => 'required|regex:/(^[-0-9A-Za-z.,\/ ]+$)/',
            'zipcode' => 'required|numeric|digits:6',
            'mobile' => 'required|numeric|digits:10',
            'date' => 'required|date|after:tomorrow',
        ]);
  
        try {

            $files = $request->file('media');
                
            $mediaFile = date('YmdHis') . "." . $files->getClientOriginalExtension();
            $files->move(public_path().'/media/files/',$mediaFile);            

            User::updateOrCreate([
                'name' => $request['name'],
                'email' => $request['email'],
                'password' => Hash::make($request['password']),
                'media' => $mediaFile,
                'address' => $request['address'],
                'zipcode' => $request['zipcode'],
                'mobile' => $request['mobile'],
                'date' => $request['date'],
            ]); 
  
            return redirect()->route('users.index')
                            ->with('success','User updated successfully');
        } catch (ModelNotFoundException $exception) {
            return back()->withError($exception->getMessage())->withInput();
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        $user->delete();
  
        return redirect()->route('users.index')
                        ->with('success','User deleted successfully');
    }
}
