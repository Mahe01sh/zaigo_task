@extends('layouts.auth')

@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <a class="btn btn-success" href="{{ route('users.create') }}"> <i class="fa fa-plus-circle" aria-hidden="true"></i> New User</a>
            </div>
        </div>
    </div><br>
   
    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif
   
    <table class="table table-bordered">
        <tr>
            <th>#</th>
            <th>Name</th>
            <th>Email</th>
            <th>Role</th>
            <th>File</th>
            <th>Address</th>
            <th>Mobile</th>
            <th>Zip Code</th>
            <th>Date & Time</th>
            <th width="180px">Action</th>
        </tr>
        @foreach ($admins as $admin)
        <tr>
            <td>{{ ++$i }}</td>
            <td>{{ $admin->name }}</td>
            <td>{{ $admin->email }}</td>
            @if($admin->is_admin == 1)
                <td>Admin</td>
            @elseif($admin->is_admin == 0)
                <td>Admin</td>
            @endif
            <td>{{ $admin->media }}</td>
            <td>{{ $admin->address }}</td>
            <td>{{ $admin->mobile }}</td>
            <td>{{ $admin->zipcode }}</td>
            <td>{{ $admin->date }}</td>
            <td>
                <form action="{{ route('admins.destroy',$admin->id) }}" method="POST">
   
                    <a class="btn btn-info" href="{{ route('admins.show',$admin->id) }}"><i class="fa fa-eye" aria-hidden="true"></i></a>
    
                    <a class="btn btn-primary" href="{{ route('admins.edit',$admin->id) }}"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
   
                    @csrf
                    @method('DELETE')
      
                    <button type="submit" onclick="return confirm('Are you sure?')" class="btn btn-danger"><i class="fa fa-trash-o" aria-hidden="true"></i></button>
                </form>
            </td>
        </tr>
        @endforeach
        @foreach ($users as $user)
        <tr>
            <td>{{ ++$i }}</td>
            <td>{{ $user->name }}</td>
            <td>{{ $user->email }}</td>
            @if($user->is_user == 1)
                <td>User</td>
            @elseif($user->is_user == 0)
                <td>User</td>
            @endif
            <td>{{ $user->media }}</td>
            <td>{{ $user->address }}</td>
            <td>{{ $user->mobile }}</td>
            <td>{{ $user->zipcode }}</td>
            <td>{{ $user->date }}</td>
            <td>
                <form action="{{ route('users.destroy',$user->id) }}" method="POST">
   
                    <a class="btn btn-info" href="{{ route('users.show',$user->id) }}"><i class="fa fa-eye" aria-hidden="true"></i></a>
    
                    <a class="btn btn-primary" href="{{ route('users.edit',$user->id) }}"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
   
                    @csrf
                    @method('DELETE')
      
                    <button type="submit" onclick="return confirm('Are you sure?')" class="btn btn-danger"><i class="fa fa-trash-o" aria-hidden="true"></i></button>
                </form>
            </td>
        </tr>
        @endforeach
    </table>
  
    {!! $users->links() !!}
      
@endsection
